# Experiments for *Optimal Binary Classification Beyond Accuracy*

This repo contains a Python implementation of simulations supporting the paper:

  Singh, S., & Khim, J. (2021). *Optimal Binary Classification Beyond Accuracy*. Accepted to Neural Information Processing Systems (NeurIPS).

A preprint of the paper is available on [ArXiv](https://arxiv.org/abs/2107.01777).

To reproduce Figures 1 and 2 of that paper:
1) Install [Python 3.9](https://www.python.org/downloads/release/python-390/).
2) Run `pip install -r requirements.txt`. You may want to do this within a [virtual environment](https://docs.python.org/3/tutorial/venv.html).
3) To reproduce Experiment 1, run `python experiment1.py`. To reproduce Experiment 2, run `python experiment2.py`. To reproduce Figure 1, run `python threshold_plot.py`.

Each experiment took about 10 minutes to run on an Ubuntu desktop with Intel Core i5-9600 CPU.
