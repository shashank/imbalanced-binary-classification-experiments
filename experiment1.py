"""This experiment demonstrates an example in which stochastic classifiers can
   achieve Bayes optimality, while deterministic classifiers cannot."""

import matplotlib.pyplot as plt
import numpy as np
from sklearn.ensemble import RandomForestClassifier
from sklearn.neighbors import KNeighborsClassifier
from tqdm import tqdm

plt.rc('font', size=20)  # Increase matplotlib default font size
np.random.seed(0)  # Fix random seed for reproducibility

np.set_printoptions(precision=3, linewidth=200)

# Simulation parameters
num_replicates = 100  # Number of independent replicates of experiment
ns = np.logspace(2, 4, 10).astype(int)  # Training sample sizes
n_test = 1000  # Size of test sample
eps = 0.1

# Thresholding parameters
deterministic_threshold = lambda z, t: z > t
stochastic_threshold = lambda z, t, p: (z > t + eps) | \
    ((np.abs(z - t) < eps) & (p > np.random.uniform(0, 1, z.shape)))
ts = np.linspace(0, 1, 100)  # Deterministic thresholds
ps = np.linspace(0, 1, 100)  # Stochastic thresholds

# Classification performance metric
metric = lambda a, b: (np.mean(a & b) * np.mean((~a) & (~b)))
optimal = (5/12)**2

# Parameters of data distribution
n_groups = 3
X_generator = lambda n : np.random.randint(0, n_groups, (n, 1))/(n_groups-1)
eta = lambda x: x.squeeze()

# Allocate space for results
deterministic_regret = np.zeros((len(ns), num_replicates))
stochastic_regret = np.zeros((len(ns), num_replicates))
random_forest_regret = np.zeros((len(ns), num_replicates))

for n_idx, n_train in enumerate(ns):
  k = int(0.1*n_train**(2/3))  # Number of nearest neighbors
  print(f'Sample size n: {n_train},  kNN parameter k: {k}')

  for i in tqdm(range(num_replicates)):

    # Generate training data
    X_train = X_generator(n_train)
    y_train = np.random.binomial(1, eta(X_train))

    # Train k-nearest neighbor classifier
    knn = KNeighborsClassifier(n_neighbors=k)
    knn.fit(X_train, y_train)
    eta_hat = 1 - knn.predict_proba(X_train)[:, 0]

    # Train random forest classifier
    random_forest = RandomForestClassifier()
    random_forest.fit(X_train, y_train)

    best_deterministic_score = 0
    best_deterministic_t = 0
    best_stochastic_score = 0
    best_stochastic_t = 0
    best_stochastic_p = 0
    scores = []
    for t in ts:
      new_deterministic_score = metric(y_train,
                                       deterministic_threshold(eta_hat, t))
      if new_deterministic_score > best_deterministic_score:
        best_deterministic_score = new_deterministic_score
        best_deterministic_t = t
      scores.append(best_deterministic_score)
      for p in ps:
        new_stochastic_score = metric(y_train,
                                      stochastic_threshold(eta_hat, t, p))
        if new_stochastic_score > best_stochastic_score:
          best_stochastic_score = new_stochastic_score
          best_stochastic_t = t
          best_stochastic_p = p

    # Estimate performance on test data
    X_test = X_generator(n_test)
    y_test = np.random.binomial(1, eta(X_test))
    eta_hat_test = 1 - knn.predict_proba(X_test)[:, 0]
    deterministic_regret[n_idx, i] = optimal - metric(
        y_test, deterministic_threshold(eta_hat_test, best_deterministic_t))
    stochastic_regret[n_idx, i] = optimal - metric(y_test, stochastic_threshold(
        eta_hat_test, best_stochastic_t, best_stochastic_p))
    random_forest_regret[n_idx, i] = optimal - metric(y_test,
        random_forest.predict(X_test) > 0)

plt.errorbar(ns,
             deterministic_regret.mean(axis=1),
             yerr=1.96*deterministic_regret.std(axis=1)/num_replicates**(1/2),
             label='Deterministic $k$-NN'),
plt.errorbar(ns,
             stochastic_regret.mean(axis=1),
             yerr=1.96*stochastic_regret.std(axis=1)/num_replicates**(1/2),
             label='Stochastic $k$-NN')
plt.errorbar(ns,
             random_forest_regret.mean(axis=1),
             yerr=1.96*random_forest_regret.std(axis=1)/num_replicates**(1/2),
             label='Det. Random Forest')
plt.plot(ns, np.zeros(ns.shape), ls='--', c='red')
plt.gca().set_xscale('log')
plt.xlabel('Sample Size ($n$)')
plt.ylabel('Regret')
plt.legend()
plt.tight_layout()
plt.show()
