"""This module generates Figure 1 of the main paper, illustrating different
   thresholding procedures."""
import matplotlib.pyplot as plt
import numpy as np

plt.rc('font', size=22)  # Increase matplotlib default font size

xs = np.linspace(0, 1, 101)

# Classical Bayes classifier
plt.subplot(1, 4, 1)
plt.title('Classical Bayes')
f_Bayes = lambda x : x > .5
plt.plot(xs[:50], f_Bayes(xs[:50]), c='b', lw=3)
plt.plot(xs[51:], f_Bayes(xs[51:]), c='b', lw=3)
plt.scatter([xs[50]], [0], c='b', s=200)
plt.scatter([xs[50]], [1], facecolors='none', edgecolor='b', s=200)
plt.ylabel('$P[\hat{Y}(x) = 1]$')

# Deterministic thresholding (Narasimhan et al. (2014), Koyejo et al. (2014))
plt.subplot(1, 4, 2)
plt.title('Deterministic')
f_deterministic = lambda x : x > .3
plt.plot(xs[:30], f_deterministic(xs[:30]), c='b', lw=3)
plt.plot(xs[31:], f_deterministic(xs[31:]), c='b', lw=3)
plt.scatter([xs[30]], [0], c='b', s=200)
plt.scatter([xs[30]], [1], facecolors='none', edgecolor='b', s=200)

# Mixtures of deterministic classifiers (Wang et al. (2019))
plt.subplot(1, 4, 3)
plt.title('Mixture of\nDeterministic')
f_mixture = lambda x : 0.3*(x > .2) + 0.7*(x > .4)
plt.plot(xs[:20], f_mixture(xs[:20]), c='b', lw=3)
plt.plot(xs[21:40], f_mixture(xs[21:40]), c='b', lw=3)
plt.plot(xs[41:], f_mixture(xs[41:]), c='b', lw=3)
plt.scatter([xs[20]], [0], c='b', s=200)
plt.scatter([xs[20]], [0.3], facecolors='none', edgecolor='b', s=200)
plt.scatter([xs[40]], [0.3], c='b', s=200)
plt.scatter([xs[40]], [1], facecolors='none', edgecolor='b', s=200)

# Stochastic Thresholding (ours)
plt.subplot(1, 4, 4)
plt.title('Stochastic (ours)')
f_stochastic = lambda x : x > .3
plt.plot(xs[:30], f_stochastic(xs[:30]), c='b', lw=3)
plt.plot(xs[31:], f_stochastic(xs[31:]), c='b', lw=3)
plt.scatter([xs[30]], [0], facecolors='none', edgecolor='b', s=200)
plt.scatter([xs[30]], [0.75], c='b', s=200)
plt.scatter([xs[30]], [1], facecolors='none', edgecolor='b', s=200)


for i in range(1, 5):
  plt.subplot(1, 4, i)
  plt.xlabel('$\eta(x)$')
  plt.grid(True)

plt.show()
