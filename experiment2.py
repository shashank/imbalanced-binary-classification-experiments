"""This experiment demonstrates that making classifiers robust to severe class
   imbalance require distinguishing different sub-types of class imbalance, such
   as Uniform Class Imbalance."""

import matplotlib.pyplot as plt
import numpy as np
from sklearn.metrics import f1_score
from sklearn.neighbors import KNeighborsClassifier
from tqdm import tqdm

plt.rc('font', size=20)  # Increase matplotlib default font size
np.random.seed(0)  # Fix random seed for reproducibility

# Simulation parameters
ns = np.logspace(2, 4, 10).astype(int)  # Training sample sizes
n_test = 1000  # Size of test sample
thresholds = np.linspace(0, 0.2, 100)  # Thresholds for computing F1 score
num_replicates = 100  # Number of independent replicates of experiment
eta1 = lambda x: r*(1 - x)
eta2 = lambda x: np.maximum(0, 1 - x/r)

# Regression error metrics
L_infinity_error = lambda x, y: np.abs(x - y).max()
L_1_error = lambda x, y: np.abs(x - y).mean()

# Allocate space for results
L_infinity_errors = np.zeros((len(ns), 2, num_replicates))
L_1_errors = np.zeros((len(ns), 2, num_replicates))
f1_scores = np.zeros((len(ns), 2, num_replicates))
bayes_f1_scores = np.zeros((len(ns), 2, num_replicates))

## RUN SIMULATION
for n_idx, n in enumerate(ns):
  r = n**(-1/2)  # Proportion of positive samples
  k = int(n**(2/3)/r**(1/3))  # Number of nearest neighbors
  print(f'Sample size n: {n},  kNN parameter k: {k}')

  for i in tqdm(range(num_replicates)):
    
    # Generate classification dataset
    X_train = np.random.uniform(0, 1, n)
    y1_train = np.random.binomial(1, eta1(X_train))
    y2_train = np.random.binomial(1, eta2(X_train))
    
    # Initialize k-nearest neighbor classifiers
    knn1 = KNeighborsClassifier(n_neighbors=k)
    knn1.fit(X_train.reshape(n, 1), y1_train)
    knn2 = KNeighborsClassifier(n_neighbors=k)
    knn2.fit(X_train.reshape(n, 1), y2_train)
    
    # Estimate regression functions over a test sample
    X_test = np.random.uniform(0, 1, n_test)
    y1_test = np.random.binomial(1, eta1(X_test))
    y2_test = np.random.binomial(1, eta2(X_test))
    eta1_hat = 1 - knn1.predict_proba(X_test.reshape(n_test, 1))[:, 0]
    eta2_hat = 1 - knn2.predict_proba(X_test.reshape(n_test, 1))[:, 0]

    # Estimate errors/performance metrics over the test sample
    L_infinity_errors[n_idx, 0, i] = L_infinity_error(eta1_hat, eta1(X_test))
    L_infinity_errors[n_idx, 1, i] = L_infinity_error(eta2_hat, eta2(X_test))
    L_1_errors[n_idx, 0, i] = L_1_error(eta1_hat, eta1(X_test))
    L_1_errors[n_idx, 1, i] = L_1_error(eta2_hat, eta2(X_test))

    for threshold in thresholds:
      f1_scores[n_idx, 0, i] = max(f1_scores[n_idx, 0, i],
                                   f1_score(y1_test, eta1_hat > threshold))
      f1_scores[n_idx, 1, i] = max(f1_scores[n_idx, 1, i],
                                   f1_score(y2_test, eta2_hat > threshold))
      bayes_f1_scores[n_idx, 0, i] = max(
          bayes_f1_scores[n_idx, 0, i],
          f1_score(y1_test, eta1(X_test) > threshold))
      bayes_f1_scores[n_idx, 1, i] = max(
          bayes_f1_scores[n_idx, 1, i],
          f1_score(y2_test, eta2(X_test) > threshold))

## PLOT RESULTS
# Uniform (L_infinity) errors
plt.subplot(1, 3, 1)
plt.errorbar(
    ns,
    L_infinity_errors[:, 0, :].mean(axis=1),
    yerr=1.96*L_infinity_errors[:, 0, :].std(axis=1)/num_replicates**(1/2))
plt.errorbar(
    ns,
    L_infinity_errors[:, 1, :].mean(axis=1),
    yerr=1.96*L_infinity_errors[:, 1, :].std(axis=1)/num_replicates**(1/2))
plt.gca().set_xscale('log')
plt.gca().set_yscale('log')
plt.legend(['$\eta_1$', '$\eta_2$'])
plt.xlabel('Sample Size ($n$)')
plt.ylabel('Uniform Error ($||\eta - \hat\eta||_\infty$)')
plt.yticks(rotation=90, size=16)

# Mean (L_1) errors
plt.subplot(1, 3, 2)
plt.errorbar(ns,
             L_1_errors[:, 0, :].mean(axis=1),
             yerr=1.96*L_1_errors[:, 0, :].std(axis=1)/num_replicates**(1/2))
plt.errorbar(ns,
             L_1_errors[:, 1, :].mean(axis=1),
             yerr=1.96*L_1_errors[:, 1, :].std(axis=1)/num_replicates**(1/2))
plt.gca().set_xscale('log')
plt.gca().set_yscale('log')
plt.xlabel('Sample Size ($n$)')
plt.ylabel('Mean Error ($||\eta - \hat\eta||_1$)')
plt.yticks(rotation=90, size=16)

# F1 scores
plt.subplot(1, 3, 3)
f1_regret = np.abs(bayes_f1_scores - f1_scores)
plt.errorbar(ns,
             f1_regret[:, 0, :].mean(axis=1),
             yerr=1.96*f1_regret[:, 0, :].std(axis=1)/num_replicates**(1/2))
plt.errorbar(ns,
             f1_regret[:, 1, :].mean(axis=1),
             yerr=1.96*f1_regret[:, 1, :].std(axis=1)/num_replicates**(1/2))
plt.gca().set_xscale('log')
plt.gca().set_yscale('log')
plt.xlabel('Sample Size ($n$)')
plt.ylabel('$F_1$ Regret')
plt.yticks(rotation=90, size=16)

plt.show()
